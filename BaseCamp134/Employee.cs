﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseCamp134
{
    class Employee
    {
        static void Main(string[] args)
        {
            int operation,number;
            //Console.WriteLine("Enter the no of employees");
            //number = Convert.ToInt32(Console.ReadLine());
            EmployeeDetails[] employeeDetails = new EmployeeDetails[0];
            do
            {
                Console.WriteLine("Enter 1 for create Employee");
                Console.WriteLine("Enter 2 for Update employee address with name");
                Console.WriteLine("Enter 3 for Display");

                Console.WriteLine("Enter the operation");
                operation = Convert.ToInt32(Console.ReadLine());

         
                switch (operation)
                {
                    case 1:
                        Console.WriteLine("Enter the no of employees");
                        number = Convert.ToInt32(Console.ReadLine());
                        employeeDetails = new EmployeeDetails[number];
                        for (int i = 0; i < number; i++)
                        {
                            EmployeeDetails employeeDetail = new EmployeeDetails();
                            Console.WriteLine($"Enter the {i + 1} Employeeid");
                            employeeDetail.EmployeeId = Convert.ToInt32(Console.ReadLine());

                            Console.WriteLine($"Enter the {i + 1} Employeename");
                            employeeDetail.EmployeeName = Console.ReadLine();

                            Console.WriteLine($"Enter the {i + 1} EmployeeDepartmentName");
                            employeeDetail.DepartmentName = Console.ReadLine();

                            Console.WriteLine($"Enter the {i + 1} EmployeeDOB");
                            employeeDetail.DOB = Console.ReadLine();

                            Console.WriteLine($"Enter the {i + 1} EmployeeAddress");
                            employeeDetail.Address = Console.ReadLine();

                            Console.WriteLine($"Enter the {i + 1} EmployeeSalary");
                            employeeDetail.Salary = Convert.ToInt32(Console.ReadLine());

                            Console.WriteLine($"Enter the {i + 1} EmployeeEmailId");
                            employeeDetail.EmailId = Console.ReadLine();

                            employeeDetails[i] = employeeDetail;
                        }
                        break;

                    case 2:
                        UpdateAddress(employeeDetails);
                        break;

                    case 3:
                        Console.WriteLine("Enter 1 for display ");
                        Console.WriteLine("enter 2 for sort employees");
                        Console.WriteLine("enter 3 for Display employee highest salary");

                        Console.WriteLine("enter the operation");
                        int find = Convert.ToInt32(Console.ReadLine());
                        if(find==1)
                        {
                            Display(employeeDetails);
                        }
                        else if (find == 2)
                        {
                            EmployeeDetails[] employeeDetails1 = new EmployeeDetails[employeeDetails.Length];
                            employeeDetails1=SortEmployees(employeeDetails);
                            for(int i=0;i<employeeDetails.Length;i++)
                            {
                                Console.WriteLine(employeeDetails1[i].EmployeeId + " " + employeeDetails1[i].EmployeeName + " " + employeeDetails1[i].DepartmentName + " " + employeeDetails1[i].DOB + " " + employeeDetails1[i].Address + " " + employeeDetails1[i].Salary + " " + employeeDetails1[i].EmailId);
                            }
                        }
                        else if (find == 3)
                        {
                            DisplayHighestSalary(employeeDetails);
                        }
                        else
                        {
                            Console.WriteLine("You entered wrong");
                        }
                        break;
                }
            } while (operation <= 3);
        }

        public static void UpdateAddress(EmployeeDetails[] employeeDetails)
        {
            Console.WriteLine("Enter the name for update the address");
            string name = Console.ReadLine();
            for(int i=0;i<employeeDetails.Length;i++)
            {
                if(name==employeeDetails[i].EmployeeName)
                {
                    Console.WriteLine("Update the address");
                    employeeDetails[i].Address = Console.ReadLine();
                }
            }
        }

        public static void Display(EmployeeDetails[] employeeDetails)
        {
            Console.WriteLine("Enter the Employee id ");
            int id = Convert.ToInt32(Console.ReadLine());

            int first, last, middle;
            first = 0;
            last = employeeDetails.Length - 1;
            middle = (first + last) / 2;

            while(first<=last)
            {
                if(employeeDetails[middle].EmployeeId<id)
                {
                    first = middle + 1;
                }
                else if(employeeDetails[middle].EmployeeId==id)
                {
                    Console.WriteLine($"{middle + 1} is the postion");
                    Console.WriteLine(employeeDetails[middle].EmployeeName + " " + employeeDetails[middle].DOB + " " + employeeDetails[middle].Address + " " + employeeDetails[middle].DOB + " " + employeeDetails[middle].Salary);
                    break;
                }
                else
                {
                    last = middle - 1;
                    middle = (first +last)/2;
                }
            }
           
        }

        public static EmployeeDetails[] SortEmployees(EmployeeDetails[] employeeDetails)
        {
            EmployeeDetails[] temp = new EmployeeDetails[employeeDetails.Length];
            for(int i=0;i<employeeDetails.Length;i++)
            {
                for(int j=0;j<employeeDetails.Length-1;j++)
                {
                    if(employeeDetails[j].DepartmentName.CompareTo(employeeDetails[j].DepartmentName)>0)
                    {
                        temp[0] = employeeDetails[j];
                        employeeDetails[j] = employeeDetails[j + 1];
                        employeeDetails[j + 1] = temp[0];
                    }
                }
            }
            return employeeDetails;
        }

        public static void DisplayHighestSalary(EmployeeDetails[] employeeDetails)
        {
            int highest = employeeDetails[0].Salary;

            for(int i=0;i<employeeDetails.Length;i++)
            {
                if(employeeDetails[i].Salary>highest)
                {
                    highest = employeeDetails[i].Salary;
                }
            }

            Console.WriteLine($"The heighest salary is {highest}");
        }
    }
}
