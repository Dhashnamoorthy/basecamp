﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseCamp134
{
    class EmployeeDetails
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string DepartmentName { get; set; }
        public string DOB { get; set; }
        public string Address { get; set; }
        public int Salary { get; set; }
        public string EmailId { get; set; }

    }
}
